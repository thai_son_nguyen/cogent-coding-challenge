package co.cogent.strategies;

import co.cogent.strategies.impl.ByteCompareStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ByteCompareStrategyTest {
    private ByteCompareStrategy simpleComparer;

    @BeforeEach
    public void setup() {
        simpleComparer = new ByteCompareStrategy();
    }

    @Test
    public void whenTwoImagesHaveDifferenContentThenTheyAreNotIdentical() throws IOException {
        var imageFolderPath = "strategies/differentcontent/";
        var image1 = new FileInputStream(new File(
                getClass().getClassLoader().getResource(imageFolderPath.concat("image_1.jpg")).getFile()
        ));
        var image2 = new FileInputStream(new File(
                getClass().getClassLoader().getResource(imageFolderPath.concat("image_2.jpg")).getFile()
        ));

        var result = simpleComparer.areIdentical(image1, image2);

        Assertions.assertEquals(false, result, "Images should not be considered identical");
    }

    @Test
    public void whenTwoImagesHaveTheSameContentThenTheyAreIdentical() throws IOException {
        var imageFolderPath = "strategies/identical/";
        var image1 = new FileInputStream(new File(
                getClass().getClassLoader().getResource(imageFolderPath.concat("image_1.jpg")).getFile()
        ));
        var image2 = new FileInputStream(new File(
                getClass().getClassLoader().getResource(imageFolderPath.concat("image_2.jpg")).getFile()
        ));

        var result = simpleComparer.areIdentical(image1, image2);

        Assertions.assertEquals(true, result,"Images should be considered identical");
    }

    @Test
    public void whenThereAreExceptionThenTwoImagesConsideredDifferent() throws IOException {
        var imageFolderPath = "strategies/identical/";
        var image1 = new FileInputStream(new File(
                getClass().getClassLoader().getResource(imageFolderPath.concat("image_1.jpg")).getFile()
        ));
        var image2 = new FileInputStream(new File(
                getClass().getClassLoader().getResource(imageFolderPath.concat("image_2.jpg")).getFile()
        ));

        image1.close();
        image2.close();

        var result = simpleComparer.areIdentical(image1, image2);

        Assertions.assertEquals(false, result,"Images should be considered different");
    }

    @Test
    public void whenTheFirstInputStreamIsNullThenTwoImagesConsideredDifferent() throws IOException {
        var imageFolderPath = "strategies/identical/";
        var image2 = new FileInputStream(new File(
                getClass().getClassLoader().getResource(imageFolderPath.concat("image_2.jpg")).getFile()
        ));

        var result = simpleComparer.areIdentical(null, image2);

        Assertions.assertEquals(false, result, "Images should be considered different");
    }

    @Test
    public void whenTheSecondInputStreamIsNullThenTwoImagesConsideredDifferent() throws IOException {
        var imageFolderPath = "strategies/identical/";
        var image1 = new FileInputStream(new File(
                getClass().getClassLoader().getResource(imageFolderPath.concat("image_1.jpg")).getFile()
        ));

        var result = simpleComparer.areIdentical(image1, null);

        Assertions.assertEquals(false, result, "Images should be considered different");
    }
}
