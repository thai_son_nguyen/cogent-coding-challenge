package co.cogent.models;

import org.junit.jupiter.api.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.zip.ZipFile;

public class ImageFeedTest {

    @Nested
    public class ForEach {
        private ImageFeed imageFeed;
        private ZipFile zipFile;

        @BeforeEach
        public void setup() throws IOException {
            var zipResource = getClass().getClassLoader().getResource("models/imagefeed/test.zip");
            zipFile = new ZipFile(zipResource.getFile());
            imageFeed = ImageFeed.fromZipFile(zipFile);
        }

        @AfterEach
        public void tearDown() throws IOException {
            zipFile.close();
        }

        @Test
        public void shouldCallHandlerForEachFileEntry() {
            var zipEntries = zipEntries(zipFile);
            var processedEntries = new HashSet<String>();
            Consumer<ImageEntry> handler = (entry) -> processedEntries.add(entry.getName());

            imageFeed.forEach(handler);

            allEntriesProcessed(zipEntries, processedEntries);
        }

        @Test
        public void whenThereIsExceptionThenGetInputStreamHandlerShouldReturnNull() throws IOException {
            var processedEntry = new AtomicReference<ImageEntry>();

            Consumer<ImageEntry> handler = processedEntry::set;

            imageFeed.forEach(handler);
            zipFile.close();

            Assertions.assertNull(processedEntry.get().getInputStream());
        }

        @Test
        public void whenThereIsExceptionThenGetInputStreamHandlerShouldReturnInputStream() {
            var processedEntry = new AtomicReference<ImageEntry>();

            Consumer<ImageEntry> handler = processedEntry::set;

            imageFeed.forEach(handler);

            Assertions.assertNotNull(processedEntry.get().getInputStream());
        }

        private void allEntriesProcessed(Set<String> expected, Set<String> actuals) {
            Assertions.assertEquals(expected.size(), actuals.size());
            Assertions.assertTrue(actuals.containsAll(expected));
        }

        private Set<String> zipEntries(ZipFile zip) {
            var fileEntries = new HashSet<String>();
            var zipEntries = zip.entries();

            while(zipEntries.hasMoreElements()) {
                var entry = zipEntries.nextElement();
                if (!entry.isDirectory()) fileEntries.add(entry.getName());
            }

            return fileEntries;
        }
    }
}