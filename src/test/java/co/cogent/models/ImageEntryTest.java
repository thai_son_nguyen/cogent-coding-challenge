package co.cogent.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.InputStream;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ImageEntryTest {
    private String name;
    private ImageEntry imageEntry;
    private InputStream inputStream;

    @BeforeEach
    public void setup() {
        inputStream = Mockito.mock(InputStream.class);
        Supplier<InputStream> getInputStreamFunc = () -> inputStream;
        name = "abc";
        imageEntry = new ImageEntry(name, getInputStreamFunc);
    }

    @Test
    public void whenGetInputStreamThenItWillReturnInputStream() {
        var actualStream = imageEntry.getInputStream();

        Assertions.assertSame(inputStream, actualStream);
    }

    @Test
    public void whenAddDuplicateEntryNameThenItWouldStoreDuplicateImageName() {
        var duplicateFileName = name + "duplicate";

        imageEntry.addDuplicateEntryName(duplicateFileName);

        Assertions.assertTrue(imageEntry.duplicateEntryNames()
                .collect(Collectors.toList()).contains(duplicateFileName));
    }
}
