package co.cogent.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class DuplicateCheckResultTest {
    private ImageCollector imageCollector;

    @BeforeEach
    public void setup() {
        imageCollector = Mockito.mock(ImageCollector.class);
    }

    @Test
    public void whenThereIsNoDuplicationInImageCollectorThenResultShouldBeEmpty() {
        var notDuplicateEntry = Mockito.mock(ImageEntry.class);
        Mockito.when(notDuplicateEntry.duplicateEntryNames()).thenReturn(Stream.empty());
        Mockito.when(imageCollector.getEntries()).thenReturn(Stream.of(notDuplicateEntry));

        var result = DuplicateCheckResult.from(imageCollector);

        Assertions.assertEquals(0, result.getDuplicates().size());
    }

    @Test
    public void whenThereIsDuplicationInImageCollectorThenResultShouldIncludeTheDuplicates() {
        var duplicateEntry = Mockito.mock(ImageEntry.class);
        var duplicates = List.of("duplicate1", "duplicate2");
        Mockito.when(duplicateEntry.getName()).thenReturn("image1");
        Mockito.when(duplicateEntry.duplicateEntryNames()).thenReturn(duplicates.stream());
        Mockito.when(imageCollector.getEntries()).thenReturn(Stream.of(duplicateEntry));
        var expectedDuplicates = new ArrayList<String>();
        expectedDuplicates.add(duplicateEntry.getName());
        expectedDuplicates.addAll(duplicates);

        var result = DuplicateCheckResult.from(imageCollector);

        Assertions.assertEquals(1, result.getDuplicates().size());
        duplicateResultIsCorrect(expectedDuplicates, result.getDuplicates().get(0));
    }

    private void duplicateResultIsCorrect(List<String> expectedDuplicates, List<String> actualDuplicates) {
        Assertions.assertEquals(expectedDuplicates.size(), actualDuplicates.size());
        expectedDuplicates.forEach((exd) -> Assertions.assertTrue(actualDuplicates.contains(exd)));
    }
}
