package co.cogent.models;

import co.cogent.strategies.impl.ByteCompareStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.zip.ZipFile;

public class ImageDuplicateCheckerTest {

    @Test
    public void whenThereIsNoDuplicateThenNoDuplicateIncludedInResult() throws IOException {
        var zipResource = getClass().getClassLoader().getResource("models/imageduplicatechecker/test_no_duplicate.zip");

        var duplicateChecker = new ImageDuplicateChecker(new ByteCompareStrategy());

        var result = duplicateChecker.check(new ZipFile(zipResource.getFile()));
        var actualDuplicates = result.getDuplicates();

        Assertions.assertTrue(actualDuplicates.isEmpty());
    }

    @Test
    public void whenThereAreDuplicatesThenAllDuplicateImagesWillBeIncludedInResult() throws IOException {
        var zipResource = getClass().getClassLoader().getResource("models/imageduplicatechecker/test.zip");
        var expectedDuplicates = expectedDuplicates();
        var duplicateChecker = new ImageDuplicateChecker(new ByteCompareStrategy());

        var result = duplicateChecker.check(new ZipFile(zipResource.getFile()));
        var actualDuplicates = result.getDuplicates();

        Assertions.assertEquals(expectedDuplicates.size(), actualDuplicates.size(), "Number of actual duplicates is not as expected");
        for (var i = 0; i < expectedDuplicates().size(); i++) {
            correctDuplicatesFound(expectedDuplicates.get(i), actualDuplicates.get(i));
        }
    }

    private void correctDuplicatesFound(List<String> expected, List<String> actual) {
        Assertions.assertEquals(expected.size(), actual.size(), "Number of actual duplicates is not as expected");
        expected.forEach((e) -> {
            Assertions.assertTrue(actual.contains(e), "Expected duplicate not found");
        });
    }

    private List<List<String>> expectedDuplicates() {
        return List.of(
                List.of("Code Test/germany/what is this_.jpg","Code Test/Dec 2016/castle from drone.jpg"),
                List.of("Code Test/germany/is this real.jpg","Code Test/Dec 2016/incredible.jpg"),
                List.of("Code Test/random cats I saw/mew.jpg","Code Test/mew.jpg"),
                List.of("Code Test/work/cat.jpg","Code Test/random cats I saw/laptop does run pretty hot.jpg"),
                List.of("Code Test/trip to the sea/s-06927.jpg","Code Test/sea, sand, surf/coves.jpg"),
                List.of("Code Test/trip to the sea/s-23225.jpg","Code Test/sea, sand, surf/ilusion.jpg"),
                List.of("Code Test/trip to the sea/s-02979.jpg","Code Test/sea, sand, surf/low tide.jpg"),
                List.of("Code Test/trip to the sea/s-08913.jpg","Code Test/sea, sand, surf/matin.jpg"),
                List.of("Code Test/trip to the sea/s-08369.jpg","Code Test/sea, sand, surf/quite a view.jpg"),
                List.of("Code Test/sea, sand, surf/turtle omg.jpg","Code Test/trip to the sea/s-08712 (turtle).jpg","Code Test/sea, sand, surf/s-01324.jpg"),
                List.of("Code Test/trip to the sea/s-02141.jpg","Code Test/sea, sand, surf/sunset.jpg"),
                List.of("Code Test/trip to the sea/s-37293.jpg","Code Test/sea, sand, surf/that starfish again.jpg"),
                List.of("Code Test/trip to the sea/s-08214.jpg","Code Test/sea, sand, surf/tree.jpg")
        );
    }
}
