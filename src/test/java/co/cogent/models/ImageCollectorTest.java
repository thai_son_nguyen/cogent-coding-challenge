package co.cogent.models;

import co.cogent.strategies.ImageCompareStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.InputStream;

public class ImageCollectorTest {
    @Nested
    public class AddImageEntry {
        private ImageCollector collector;
        private ImageCompareStrategy compareStrategy;

        @BeforeEach
        public void setup() {
            compareStrategy = Mockito.mock(ImageCompareStrategy.class);
            collector = new ImageCollector(compareStrategy);
        }

        @Test
        public void whenThereIsNoExistingEntryThenNewEntryIsNotConsideredADuplicate() {
            var newEntry = Mockito.mock(ImageEntry.class);
            Mockito.when(newEntry.getName()).thenReturn("entry1");
            Mockito.when(compareStrategy.areIdentical(Mockito.any(), Mockito.any())).thenReturn(true);

            collector.addEntry(newEntry);

            Assertions.assertTrue(collector.getEntries().anyMatch(e -> e == newEntry));
        }

        @Test
        public void whenThereIsExistingEntryWithDifferentContentThenNewEntryIsNotConsideredADuplicate() {
            var existingEntryInputStream = Mockito.mock(InputStream.class);
            var existingEntry = new ImageEntry("existingEntry", () -> existingEntryInputStream);
            var newEntryInputStream = Mockito.mock(InputStream.class);
            var newEntry = new ImageEntry("entry1", () -> newEntryInputStream);

            Mockito.when(compareStrategy.areIdentical(existingEntryInputStream, newEntryInputStream)).thenReturn(false);

            collector.addEntry(existingEntry);
            collector.addEntry(newEntry);

            Assertions.assertTrue(collector.getEntries().anyMatch(e -> e == newEntry));
        }

        @Test
        public void whenThereIsExistingEntryWithSameContentThenNewEntryIsADuplicate() {
            var existingEntryInputStream = Mockito.mock(InputStream.class);
            var existingEntry = new ImageEntry("existingEntry", () -> existingEntryInputStream);
            var newEntryInputStream = Mockito.mock(InputStream.class);
            var newEntry = new ImageEntry("entry1", () -> newEntryInputStream);

            Mockito.when(compareStrategy.areIdentical(existingEntryInputStream, newEntryInputStream)).thenReturn(true);

            collector.addEntry(existingEntry);
            collector.addEntry(newEntry);

            Assertions.assertFalse(collector.getEntries().anyMatch(e -> e == newEntry));
            Assertions.assertTrue(existingEntry.duplicateEntryNames().anyMatch(de -> de.equals(newEntry.getName())));
        }

    }
}
