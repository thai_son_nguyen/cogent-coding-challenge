package co.cogent.strategies;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface ImageCompareStrategy {
    boolean areIdentical(InputStream image1, InputStream image2);
}
