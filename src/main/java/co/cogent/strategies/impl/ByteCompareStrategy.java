package co.cogent.strategies.impl;

import co.cogent.strategies.ImageCompareStrategy;

import java.io.*;

public class ByteCompareStrategy implements ImageCompareStrategy {
    @Override
    public boolean areIdentical(InputStream image1, InputStream image2) {
        try {
            return haveSameContent(image1, image2);
        } catch (Exception ex) {
            return false;
        }
    }

    private boolean haveSameContent(InputStream image1, InputStream image2) throws IOException {
        var bufferedImage1 = new BufferedInputStream(image1);
        var bufferedImage2 = new BufferedInputStream(image2);

        try {
            int value1;
            int value2;
            do {
                value1 = bufferedImage1.read();
                value2 = bufferedImage2.read();
                if (value1 != value2) return false;
            } while (value1 >= 0);
        } finally {
            bufferedImage1.close();
            bufferedImage2.close();
        }
        return true;
    }
}
