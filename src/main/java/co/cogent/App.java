package co.cogent;

import co.cogent.models.DuplicateCheckResult;
import co.cogent.models.ImageDuplicateChecker;
import co.cogent.strategies.impl.ByteCompareStrategy;

import java.io.IOException;
import java.util.zip.ZipFile;

public class App
{
    public static void main( String[] args )
    {
        if(args.length == 0) {
            output("Please provide file name");
        } else {
            processZipFile(args[0]);
        }
    }

    private static void processZipFile(String path) {
        try {
            var zipFile = new ZipFile(path);
            output(duplicateChecker().check(zipFile));
        } catch (IOException e) {
            output("Unable to load file");
        }
    }

    private static ImageDuplicateChecker duplicateChecker() {
        return new ImageDuplicateChecker(new ByteCompareStrategy());
    }

    private static void output(DuplicateCheckResult result) {

        if (result.getDuplicates().size() > 0) {
            result.getDuplicates().forEach((d) -> output(String.join(", ", d)));
        } else {
            output("No duplicate");
        }
    }

    private static void output(String message) {
        System.out.println(message);
    }
}
