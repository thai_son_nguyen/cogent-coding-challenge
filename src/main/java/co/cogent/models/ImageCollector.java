package co.cogent.models;

import co.cogent.strategies.ImageCompareStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ImageCollector {
    private ImageCompareStrategy compareStrategy;

    private List<ImageEntry> entries;

    public Stream<ImageEntry> getEntries() {
        return entries.stream();
    }

    public ImageCollector(ImageCompareStrategy compareStrategy) {
        this.compareStrategy = compareStrategy;
        entries = new ArrayList<>();
    }

    public void addEntry(ImageEntry entry) {
        for (var e : entries) {
            if(compareStrategy.areIdentical(e.getInputStream(), entry.getInputStream())) {
                e.addDuplicateEntryName(entry.getName());
                return;
            }
        }
        entries.add(entry);
    }
}
