package co.cogent.models;

import java.io.InputStream;
import java.util.function.Consumer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ImageFeed {
    private ZipFile zipFile;

    private ImageFeed(ZipFile zipFile) {
        this.zipFile = zipFile;
    }

    public static ImageFeed fromZipFile(ZipFile zipFile) {
        return new ImageFeed(zipFile);
    }

    public void forEach(Consumer<ImageEntry> handler) {
        var entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            var entry = entries.nextElement();
            if (!entry.isDirectory()) {
                handler.accept(new ImageEntry(entry.getName(), () -> getInputStream(entry)));
            }
        }
    }

    private InputStream getInputStream(ZipEntry entry) {
        try {
            return zipFile.getInputStream(entry);
        } catch (Exception e) {
        }
        return null;
    }
}
