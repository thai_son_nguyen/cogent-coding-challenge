package co.cogent.models;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DuplicateCheckResult {
    private final List<List<String>> duplicates;

    public List<List<String>> getDuplicates() {
        return duplicates;
    }

    private DuplicateCheckResult(List<List<String>> duplicates) {
        this.duplicates = duplicates;
    }

    public static DuplicateCheckResult from(ImageCollector collector) {
        var duplicates = new ArrayList<List<String>>();

        collector.getEntries().forEach((entry) -> {
            var dupList = entry.duplicateEntryNames().collect(Collectors.toList());
            if (dupList.size() > 0) {
                dupList.add(entry.getName());
                duplicates.add(dupList);
            }
        });

        return new DuplicateCheckResult(duplicates);
    }
}
