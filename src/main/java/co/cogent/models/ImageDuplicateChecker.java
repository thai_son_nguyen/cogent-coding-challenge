package co.cogent.models;

import co.cogent.strategies.ImageCompareStrategy;

import java.util.zip.ZipFile;

public class ImageDuplicateChecker {
    private final ImageCompareStrategy compareStrategy;

    public ImageDuplicateChecker(ImageCompareStrategy compareStrategy) {
        this.compareStrategy = compareStrategy;
    }

    public DuplicateCheckResult check(ZipFile file) {
        var imageCollector = new ImageCollector(this.compareStrategy);
        var imageFeed = ImageFeed.fromZipFile(file);

        imageFeed.forEach(imageCollector::addEntry);

        return DuplicateCheckResult.from(imageCollector);
    }
}
