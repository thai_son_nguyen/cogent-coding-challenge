package co.cogent.models;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class ImageEntry {
    private Supplier<InputStream> getInputStreamFunc;
    private String name;
    private List<String> duplicateEntryNames;

    public String getName() {
        return name;
    }

    public ImageEntry(String name, Supplier<InputStream> getInputStreamFunc) {
        this.name = name;
        this.getInputStreamFunc = getInputStreamFunc;
        this.duplicateEntryNames = new ArrayList<>();
    }

    public InputStream getInputStream() {
        return getInputStreamFunc.get();
    }

    public void addDuplicateEntryName(String name) {
        duplicateEntryNames.add(name);
    }

    public Stream<String> duplicateEntryNames() {
        return duplicateEntryNames.stream();
    }
}
