## SYSTEM REQUIREMENTS
  1. Java 11
  2. Tested in Windows
  3. Maven

## DEVELOPMENT TOOL
  1. IntelliJ IDE
  2. Maven

## RUN INSTRUCTIONS
  1. Clone the repo
  2. Run `mvn package` to create executable jar called `cogent-image-duplicate-checker.jar` under folder `target`
  3. Copy the `jar` and the archive to the same folder 
  4. Run `java -jar cogent-image-duplicate-checker.jar [file of the archive]`. For example: `java -jar cogent-image-duplicate-checker.jar myphotos.zip`

## RUN UNIT TEST INSTRUCTIONS
  1. Clone the repo
  2. Run `mvn test`
  
## APPLICATION BEHAVIOURS
  1. If archive isn't specified, output `Please provide file name`
  2. If archive isn't valid, output `Unable to load file`
  3. If there are duplicates, output duplicates in groups separated by `new line`
  4. If there isn't duplicate, output `No duplicate` 


## DESIGN CHOICES
  1. Minimal error handling
  2. Bit by bit comparison method is used to compare images
  3. Actual files are used for unit test as it's simple and easy to verify

## DESIGN PRINCIPLES
  1. Test Driven Development
  2. Domain Driven Design
  3. Dependency Injection
  4. S.O.L.I.D

## FUTURE IMPROVEMENT
  1. Improve comparision algorithm for better performance
  2. More error handlings

##NOTES
  - I haven't touched Java and Maven for a long time so all feedbacks are appreciated
  - Bitbucket is used as Git source control